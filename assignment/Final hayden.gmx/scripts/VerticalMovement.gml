/// horizontal movement h speed
var vSpeed = argument0;
var collisionObject = argument1;
var vDirection = sign(vSpeed);
if (place_meeting(x, y + vSpeed, collisionObject))
{
    for (var i = 0; i < abs(vSpeed); ++i)
    {
        if (!place_meeting(x, y + vDirection, collisionObject))
        {
            y += vDirection; 
        }
        else
        {
            break;
        }
    }
    return false;
}
y += vSpeed;
return true;
