if (keyboard_check(vk_right))
{
    sprite_index = spr_PlayerGoingRight;
}
else if (keyboard_check(vk_left))
{
    sprite_index = spr_PlayerGoingLeft;
}
else
{
    sprite_index = spr_Player;
}
