/// horizontal movement h speed
var hSpeed = argument0;
var collisionObject = argument1;
var hDirection = sign(hSpeed);
if (place_meeting(x + hSpeed, y, collisionObject))
{
    for (var i = 0; i < abs(hSpeed); ++i)
    {
        if (!place_meeting(x + hDirection, y, collisionObject))
        {
            x += hDirection; 
        }
        else
        {
            break;
        }
    }
    return false;
}
x += hSpeed;
return true;


